﻿using CrestronUI.PsConsole;
using CrestronUploadTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace CrestronUI
{
    public partial class MainWindow
    {
        private void UploadMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!IpTextBox.Text.Equals("Enter IP/Hostname"))
            {
                string programFilePath = $@"'{selectedTreeView.CurrentItem.Tag}'";
                string ipAddress = IpTextBox.Text;

                ConfirmWindow sub = new ConfirmWindow(this, ipAddress, selectedTreeView.CurrentItem.Header.ToString());
                bool? yesOrNo = sub.ShowDialog();
                if (yesOrNo == true)
                {
                    CrestronTool.Instance.UploadProgram(ipAddress, programFilePath, (FileTypes)selectedTreeView.Tag);
                }
            }
        }

        private void Instance_OnUploadCompleted(object sender, ProgramUploadArgs args)
        {
            Dispatcher.Invoke(() =>
            {
                string uploadResults = Regex.Replace(args.UploadResults, @"^\s*$\n", string.Empty, RegexOptions.Multiline);
                AppendTextToConsole($"\n\n{uploadResults}\nComplete\n\n{MyConsole.Instance.ConsoleDirectory}");
                consoleTextBlock.Select(consoleTextBlock.Text.Length, 0);
                consoleTextBlock.ScrollToEnd();
            });
        }
    }
}
