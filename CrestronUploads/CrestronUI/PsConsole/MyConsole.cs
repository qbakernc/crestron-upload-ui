﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Management.Automation;

namespace CrestronUI.PsConsole
{
    public class MyConsole
    {
        public delegate void ConsoleEventHandler(object sender, MyConsoleEventArgs args);
        public event ConsoleEventHandler OnConsoleDataReceived;

        private static MyConsole instance;
        private readonly BackgroundWorker worker = new BackgroundWorker();
        private string consoleDirectory = "C:\\Crestron";
        public static MyConsole Instance 
        {
            get
            {
                if (instance == null)
                    instance = new MyConsole();
                return instance;
            } 
        }
        public string ConsoleDirectory 
        {
            get
            {
                return $"{consoleDirectory}>";
            }
            private set{ }
        }
        private MyConsole()
        {
            worker.DoWork += Worker_DoWork;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
        }

        public void SendCmndlet(string command)
        {
            worker.RunWorkerAsync(command);
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string command = e.Argument.ToString();
            string consoleResponse = string.Empty;
            using (PowerShell ps = PowerShell.Create())
            {

                // Changes the directory to the last saved location. Have to do this because the directory resets everytime we create a new powershell object.
                ps.AddScript($"cd {consoleDirectory}");
                ps.Invoke();

                // Sends the Command
                ps.AddScript($"{command} | out-string");
                try
                {
                    Collection<PSObject> results = ps.Invoke();

                    // Tries to grab a valid response from the powershell command
                    foreach (PSObject row in results)
                        consoleResponse += $"\n{row}";

                    // If there were any errors we will grab those as well
                    foreach (ErrorRecord error in ps.Streams.Error.ReadAll())
                        consoleResponse += $"\n\n{error}\n";

                    // Sends command to get the current location.
                    ps.AddScript($"Get-Location | out-string");
                    results = ps.Invoke();

                    // Grabs just the directory and saves it in our console Directory variable.
                    string confirmDirectory = results[0].ToString();
                    int start = confirmDirectory.LastIndexOf(":") - 1;
                    int stop = confirmDirectory.IndexOf("\n",start) - 1;
                    int length = stop - start;
                    consoleDirectory = confirmDirectory.Substring(start, length);

                    // Appends the console directory to the response from our command.
                    consoleResponse += $"\n{consoleDirectory}>";
                }
                catch (Exception ex)
                {
                    consoleResponse += $"\n\n{ex}\n\n{consoleDirectory}>";
                }
            }
            e.Result = consoleResponse;
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            OnConsoleDataReceived?.Invoke(this, new MyConsoleEventArgs(e.Result.ToString()));
        }
    }
}
