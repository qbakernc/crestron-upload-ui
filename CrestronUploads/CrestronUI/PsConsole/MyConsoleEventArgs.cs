﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrestronUI.PsConsole
{
    public class MyConsoleEventArgs : EventArgs
    {
        public string ConsoleResponse { get; private set; }

        public MyConsoleEventArgs(string consoleResponse)
        {
            ConsoleResponse = consoleResponse;
        }
    }
}
