﻿using CrestronUI.ProgramUploads;
using CrestronUploadTool;
using CustomTreeViewItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrestronUI
{
    public partial class MainWindow
    {
        private void TreeView_OnTreeViewExpanded(VSCodeTreeView sender, EventArgs args)
        {
            if (selectedTreeView != null && selectedTreeView != sender)
                selectedTreeView.CollaspeTreeView(false);
            selectedTreeView = sender;
        }
        private void TreeView_RefreshButtonClicked(VSCodeTreeView sender, EventArgs args)
        {
            sender.MyItemSource = LocalFileTools.BuildFolderList((FileTypes)sender.Tag, contextMenu);
        }

        private void TreeView_CollaspeAllClicked(VSCodeTreeView sender, EventArgs args)
        {
            sender.CollaspeTreeView(true);
        }
    }
}
