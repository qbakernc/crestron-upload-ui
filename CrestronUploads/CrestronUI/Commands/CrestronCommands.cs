﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Markup;
using CustomTreeViewItem;

namespace CrestronUI.Commands
{
    class CrestronCommands
    {
        private static CrestronCommands instance;
        private readonly List<VSCodeItems> vsCodeList = new List<VSCodeItems>();
        private readonly List<string> commandList = new List<string>() 
        {
            "iptable ", "addmaster ", "ssl ", "ssl off", "reboot",
            "hostname ", "progcomments ", "progreset ", "sntp period:", "sntp start",
            "sntp ", "sntp server:", "sntp sync", "timezone ", "timezone 014" 
        };

        public static CrestronCommands Instance 
        { 
            get
            {
                if (instance == null)
                {
                    instance = new CrestronCommands();
                }
                return instance;
            }        
        }
        private CrestronCommands(){}

        public List<VSCodeItems> GetCrestronCommands(ContextMenu contextMenu)
        {
            commandList.Sort();
            foreach (string s in commandList)
            {
                VSCodeItems temp = new VSCodeItems
                {
                    Header = s,
                    ContextMenu = contextMenu,
                    CheckedImageSource = null,
                    UncheckedImageSource = null
                };
                vsCodeList.Add(temp);
            }
            return vsCodeList;
        }
    }
}
