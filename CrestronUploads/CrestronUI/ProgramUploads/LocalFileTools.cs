﻿using CustomTreeViewItem;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CrestronUploadTool;

namespace CrestronUI.ProgramUploads
{
    public static class LocalFileTools
    {
        private static readonly Dictionary<int, string> rootPath = new Dictionary<int, string>()
        {
            { 1, "C:\\Crestron\\VS2019" },
            { 2, "C:\\Crestron\\VS2008" },
            { 3, "C:\\Crestron\\Simpl" },
            { 4, "C:\\Crestron\\Simpl" },
            { 5, "C:\\Crestron\\VT Files" },
            { 6, "C:\\Crestron\\VT Files" }
        };

        private static readonly Dictionary<int, string> imagePath = new Dictionary<int, string>()
        {
            { 1, "pack://application:,,,/CrestronUI;Component/Images/VS2019Logo.png" },
            { 2, "pack://application:,,,/CrestronUI;Component/Images/Simpl.png" },
            { 3, "pack://application:,,,/CrestronUI;Component/Images/Simpl.png" },
            { 4, "pack://application:,,,/CrestronUI;Component/Images/Simpl.png" },
            { 5, "pack://application:,,,/CrestronUI;Component/Images/Simpl.png" },
            { 6, "pack://application:,,,/CrestronUI;Component/Images/Simpl.png" }
        };

        public static List<VSCodeItems> BuildFolderList(FileTypes fileType, ContextMenu contextMenu)
        {
            string[] dir = Directory.GetDirectories(rootPath[(int)fileType]);
            
            VSCodeItems[] programFolders = new VSCodeItems[dir.Length];
            for (int i = 0; i < dir.Length; i++)
            {
                // Creates a new TreeViewItem in our array and sets the Header to the name of the folderList we just gathered.
                programFolders[i] = new VSCodeItems
                {
                    Header = Path.GetFileName(dir[i]),
                    ToolTip = Path.GetFileName(dir[i]),
                    Tag = dir[i]
                };
            }
            GrabProgramFiles(programFolders, fileType, contextMenu);
            List<VSCodeItems> treeView = new List<VSCodeItems>();
            foreach (VSCodeItems tVI in programFolders)
            {
                treeView.Add(tVI);
            }
            
            return treeView;
        }
        private static void GrabProgramFiles(VSCodeItems[] programFolders, FileTypes selectedFileType, ContextMenu contextMenu
            )
        {
            string newPath = String.Empty;
            string[] programs = Array.Empty<string>();
            BitmapImage fileImage = new BitmapImage(new Uri(imagePath[(int)selectedFileType]));
            foreach (VSCodeItems folder in programFolders)
            {
                try
                {
                    switch (selectedFileType)
                    {
                        case FileTypes.CpzVS2019:
                            newPath = Directory.GetDirectories((string)folder.Tag).Where(name => !name.Contains(".git") && !name.Contains(".md")).ToArray()[0];
                            programs = Directory.GetFiles($@"{newPath}\{Path.GetFileName(newPath)}\bin\Debug").Where(name => name.EndsWith(".cpz")).ToArray();
                            break;
                        case FileTypes.CpzVS2008:
                            programs = Directory.GetFiles($@"{(string)folder.Tag}\{Path.GetFileName((string)folder.Tag)}\bin\Debug").Where(name => name.EndsWith(".cpz")).ToArray();
                            break;
                        case FileTypes.Lpz:
                            programs = Directory.GetFiles($@"{(string)folder.Tag}").Where(name => name.EndsWith(".lpz")).ToArray();
                            break;
                        case FileTypes.Spz:
                            programs = Directory.GetFiles($@"{(string)folder.Tag}").Where(name => name.EndsWith(".spz")).ToArray();
                            break;
                        case FileTypes.Tsw:
                            programs = Directory.GetFiles($@"{(string)folder.Tag}").Where(name => name.EndsWith(".vtz")).ToArray();
                            break;
                        case FileTypes.Tps:
                            programs = Directory.GetFiles($@"{(string)folder.Tag}").Where(name => name.EndsWith(".vtz")).ToArray();
                            break;
                    }
                }
                catch (Exception) { }
                foreach (string s in programs)
                {
                    VSCodeItems program = new VSCodeItems
                    {
                        Header = Path.GetFileName(s),
                        ToolTip = Path.GetFileName(s),
                        Tag = s,
                        UncheckedImageSource = fileImage,
                        ContextMenu = contextMenu
                    };
                    folder.AddItem(program);
                }
            }
        }
    }
}
