﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CrestronUI.Tools
{
    public class Utilities
    {
        public static bool IsNumberKey(Key inKey)
        {
            if (inKey >= Key.D0 && inKey <= Key.D9)
            {

                return true;
            }
            else if (inKey >= Key.NumPad0 && inKey <= Key.NumPad9)
            {
                return true;
            }
                return false;
        }
        public static bool IsLetterKey(Key inKey)
        {
            if (inKey >= Key.A && inKey <= Key.Z)
            {
                return true;
            }
            return false;
        }
        public static bool IsSeparatorKey(Key inKey)
        {
            if(inKey == Key.OemPeriod || inKey == Key.OemMinus || inKey == Key.Back || inKey == Key.Subtract || inKey == Key.Decimal || inKey == Key.Oem1)
            {
                return true;
            }
            return false;
        }
        public static bool IsArrowKey(Key inKey)
        {
            if (inKey == Key.Up || inKey == Key.Down || inKey == Key.Left || inKey == Key.Right || inKey == Key.Delete)
            {
                return true;
            }
            return false;
        }
        public static bool IsCtrlKey(Key inKey)
        {
            if (inKey == Key.LeftCtrl || inKey == Key.RightCtrl)
            {
                return true;
            }
            return false;
        }

    }
}
