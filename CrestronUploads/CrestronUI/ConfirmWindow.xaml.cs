﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CrestronUI
{
    /// <summary>
    /// Interaction logic for ConfirmWindow.xaml
    /// </summary>
    public partial class ConfirmWindow : Window
    {
        public ConfirmWindow(MainWindow owner, string selectedProcessor, string cpzFileName)
        {
            InitializeComponent();

            this.Owner = owner;
            confirmSelectionText.Text = $"Are you sure you want to upload {cpzFileName} to this Processor?";
            confirmSelectionLabel.Content = $"Selected Processor:  {selectedProcessor}";

            // Register Events
            yesButton.Click += YesButton_Click;
            noButton.Click += NoButton_Click;
            xButton.Click += XButton_Click;
        }
        private void XButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
