﻿using CrestronUI.PsConsole;
using CrestronUI.Tools;
using System;
using System.Text;
using System.Windows.Input;

namespace CrestronUI
{
    public partial class MainWindow
    {
        public StringBuilder stringBuilder = new StringBuilder();

        private void ConsoleTextBlock_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (Utilities.IsCtrlKey(e.Key))
                ctrlPressed = false; 
            else if (e.Key == Key.Back)
                backPressed = false;
        }
        private void ConsoleTextBlock_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Cut || e.Command == ApplicationCommands.Undo)
                e.Handled = true;
        }
        private void ConsoleTextBlock_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (ctrlPressed)
                consoleTextBlock.ScrollToHorizontalOffset(consoleTextBlock.HorizontalOffset + Math.Sign(e.Delta) * -30);
            else
                consoleTextBlock.ScrollToVerticalOffset(consoleTextBlock.VerticalOffset + Math.Sign(e.Delta) * -30);
        }
        private void ConsoleTextBlock_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Up)
            {
                if (!consoleTextBlock.Text.EndsWith(">") && savedCommandsIndex > 0)
                    consoleTextBlock.Text = consoleTextBlock.Text.Remove(consoleTextBlock.Text.LastIndexOf(">") + 1);
                if (savedCommandsIndex > 0)
                {
                    savedCommandsIndex--;
                    AppendTextToConsole(savedConsoleCommands[savedCommandsIndex]);
                }
            }
            else if (e.Key == Key.Down)
            {
                if (!consoleTextBlock.Text.EndsWith(">") && savedCommandsIndex < savedConsoleCommands.Count - 1)
                    consoleTextBlock.Text = consoleTextBlock.Text.Remove(consoleTextBlock.Text.LastIndexOf(">") + 1);
                if (savedCommandsIndex < savedConsoleCommands.Count - 1)
                {
                    savedCommandsIndex++;
                    AppendTextToConsole(savedConsoleCommands[savedCommandsIndex]);
                }
            }

            if (Utilities.IsCtrlKey(e.Key))
            {
                ctrlPressed = true;
            }
            else if (e.Key == Key.Back && consoleTextBlock.CaretIndex <= consoleTextBlock.Text.LastIndexOf(">") + 1)
            {
                backPressed = true;
                e.Handled = true;
            }
            else if (Utilities.IsArrowKey(e.Key) || consoleBusy)
            {
                e.Handled = true;
            }
            else if (e.Key == Key.Enter && !consoleBusy && consoleTextBlock.Text.LastIndexOf(">") != consoleTextBlock.Text.Length - 1)
            {
                string command = consoleTextBlock.Text.Substring(consoleTextBlock.Text.LastIndexOf(">") + 1);
                savedConsoleCommands.Add(command);
                if (command != "clear")
                {
                    AppendTextToConsole("\n");
                    MyConsole.Instance.SendCmndlet(command);
                    consoleBusy = true;
                }
                else
                {
                    consoleTextBlock.Clear();
                    consoleTextBlock.Text = MyConsole.Instance.ConsoleDirectory;
                }
            }

            if (!ctrlPressed && !backPressed && consoleTextBlock.CaretIndex < consoleTextBlock.Text.LastIndexOf(">") + 1)
            {
                consoleTextBlock.Select(consoleTextBlock.Text.Length, 0);
                consoleTextBlock.ScrollToEnd();
            }
        }
        private void AppendTextToConsole(string input)
        {
            stringBuilder.Append(input);
            consoleTextBlock.Text += stringBuilder.ToString();
            stringBuilder.Clear();
        }
        private void MyConsole_OnConsoleDataReceived(object sender, MyConsoleEventArgs args)
        {
            AppendTextToConsole(args.ConsoleResponse);
            consoleTextBlock.Select(consoleTextBlock.Text.Length, 0);
            consoleTextBlock.ScrollToEnd();
            consoleBusy = false;
            savedCommandsIndex = savedConsoleCommands.Count;
        }
    }
}
