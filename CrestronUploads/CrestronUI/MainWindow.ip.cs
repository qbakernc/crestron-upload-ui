﻿using CrestronUI.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CrestronUI
{
    public partial class MainWindow
    {
        private void IpTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            Trace.WriteLine(e.Key.ToString());
            e.Handled = !(Utilities.IsLetterKey(e.Key) | Utilities.IsNumberKey(e.Key) | Utilities.IsSeparatorKey(e.Key) | ((e.Key == Key.Space || e.Key == Key.OemQuestion) && IpTextBox.Text.Contains(":")));
        }
        private void IpTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (IpTextBox.Text == String.Empty)
                IpTextBox.Text = "Enter IP/Hostname";
        }

        private void IpTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (IpTextBox.Text == "Enter IP/Hostname")
                IpTextBox.Clear();
        }
    }
}
