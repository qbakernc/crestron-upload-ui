﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Management.Automation;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls.Primitives;
using CrestronUI.ProgramUploads;
using CrestronUI.Tools;
using CustomTreeViewItem;
using System.Diagnostics;
using System.Windows.Media.Animation;
using CrestronUI.PsConsole;
using CrestronUI.Commands;
using CrestronUploadTool;

namespace CrestronUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private VSCodeTreeView selectedTreeView;
        private readonly ContextMenu contextMenu = new ContextMenu();
        private readonly ContextMenu commandsContextMenu = new ContextMenu();
        private readonly MenuItem uploadItem = new MenuItem();
        private readonly MenuItem sendCommandItem = new MenuItem();
        private readonly MenuItem send2SeriesCommandItem = new MenuItem();
        private bool consoleBusy;
        private bool ctrlPressed;
        private bool backPressed;
        private List<string> savedConsoleCommands = new List<string>();
        private int savedCommandsIndex = -1;

        public MainWindow()
        {
            InitializeComponent();
            uploadItem.Header = "Upload";
            contextMenu.Items.Add(uploadItem);
            sendCommandItem.Header = "Send Command";
            send2SeriesCommandItem.Header = "Send 2-Series Command";
            commandsContextMenu.Items.Add(sendCommandItem);
            commandsContextMenu.Items.Add(send2SeriesCommandItem);
           
            // Register Events
            uploadItem.Click += UploadMenuItem_Click;
            sendCommandItem.Click += SendCommandItem_Click;
            send2SeriesCommandItem.Click += Send2SeriesCommandItem_Click;
            MyConsole.Instance.OnConsoleDataReceived += MyConsole_OnConsoleDataReceived;
            CrestronTool.Instance.OnUploadCompleted += Instance_OnUploadCompleted;
            CrestronTool.Instance.OnCommandResponseReceived += Instance_OnCommandResponseReceived;
        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            VS2019TreeView.MyItemSource = LocalFileTools.BuildFolderList(FileTypes.CpzVS2019, contextMenu);
            VS2008TreeView.MyItemSource = LocalFileTools.BuildFolderList(FileTypes.CpzVS2008, contextMenu);
            LpzTreeView.MyItemSource = LocalFileTools.BuildFolderList(FileTypes.Lpz, contextMenu);
            SpzTreeView.MyItemSource = LocalFileTools.BuildFolderList(FileTypes.Spz, contextMenu);
            TswTreeView.MyItemSource = LocalFileTools.BuildFolderList(FileTypes.Tsw, contextMenu);
            TpsTreeView.MyItemSource = LocalFileTools.BuildFolderList(FileTypes.Tps, contextMenu);
            CommandsTreeView.MyItemSource = CrestronCommands.Instance.GetCrestronCommands(commandsContextMenu);

        }
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        } 
    }
}
