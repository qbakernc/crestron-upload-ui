﻿using CrestronUI.PsConsole;
using CrestronUploadTool;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace CrestronUI
{
    public partial class MainWindow
    {
        private void SendCommandItem_Click(object sender, RoutedEventArgs e)
        {
            CrestronTool.Instance.SendCrestronCommand(IpTextBox.Text, selectedTreeView.CurrentItem.Header.ToString());
        }
        private void Send2SeriesCommandItem_Click(object sender, RoutedEventArgs e)
        {
            CrestronTool.Instance.SendCrestronCommand2Series(IpTextBox.Text, selectedTreeView.CurrentItem.Header.ToString());
            string command = selectedTreeView.CurrentItem.Header.ToString();
            string ipAddress = IpTextBox.Text;
            if (ipAddress.Contains(":"))
            {
                command += ipAddress.Substring(ipAddress.IndexOf(":") + 1);
                ipAddress = ipAddress.Substring(0, ipAddress.IndexOf(":"));
            }
            Trace.WriteLine(command);
            Trace.WriteLine(ipAddress);

        }

        private void Instance_OnCommandResponseReceived(object sender, CrestronCommandsEventArgs args)
        {
            Dispatcher.Invoke(() =>
            {
                string uploadResults = Regex.Replace(args.CommandResults, @"^\s*$\n", string.Empty, RegexOptions.Multiline);
                AppendTextToConsole($"\n\n{uploadResults}\nComplete\n\n{MyConsole.Instance.ConsoleDirectory}");
                consoleTextBlock.Select(consoleTextBlock.Text.Length, 0);
                consoleTextBlock.ScrollToEnd();
            });
        }
    }
}
